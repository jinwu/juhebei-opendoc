# 聚合配送开放文档



### 配送店铺管理

#### 1. **查询配送门店业务类型**

`POST` [https://open.juhesaas.com/api/delivery/shop/businessCategory/list](https://open.juhesaas.com/api/delivery/shop/businessCategory/list)

- **_请求说明_**  
  
  > 无请求参数

- **_响应说明_**  

| 响应参数    | 名称   | 类型     | 备注                |
| ------- | ---- | ------ | ----------------- |
| code    | 响应码  | int    | 0表示请求成功，其它表示失败    |
| message | 响应消息 | string | 响应码对应的说明          |
| data    |      |        |                   |
| - id    | 业态id | int    | 配送店铺业务类型id， 长度为2位 |
| - desc  | 业态名称 | string | 配送店铺业务类型名称        |

_示例：_

```java
{
  "code": 0,
  "message": "ok",
  "data": 
      [
        {"id":"1","desc":"快餐"},
        {"id":"2","desc":"水果生鲜"},
        {"id":"3","desc":"OTC药品"}
      ]
}
```

- **_使用说明_**
  
  > 1. 新增店铺后，店铺业态不可修改，请尽量选择贴合您业务的业态id  

---

#### 2. **查询配送渠道列表**

`POST` [https://open.juhesaas.com/api/delivery/shop/channel/list](https://open.juhesaas.com/api/delivery/shop/channel/list)

- **_请求说明_**  

| 请求参数   | 名称   | 类型   | 是否必填 | 备注            |
| ------ | ---- | ---- | ---- | ------------- |
| shopId | 店铺id | long | 否    | 创建店铺接口返回的店铺id |

 _示例：_

```java
{
  "shopId": 985
}
```

- **_响应说明_**  

| 响应参数          | 名称       | 类型     | 备注                 |
| ------------- | -------- | ------ | ------------------ |
| code          | 响应码      | int    | 0 表示请求成功，其它表示失败    |
| message       | 响应消息     | string | 响应码对应的说明           |
| data          |          |        |                    |
| - id          | 配送渠道服务id | string |                    |
| - channelType | 渠道类型     | string |                    |
| - canAddTips  | 是否支持增加小费 | int    | 0 不支持 1 支持         |
| - name        | 配送渠道名称   | string |                    |
| - logo        | 配送渠道logo | string |                    |
| - status      | 渠道开通状态   | string | -1从未开通 1已开通 3开通失败过 |

_示例：_

```java
{
    "data": [{
            "id": "10731",
            "channelType": "731",
            "canAddTips": 0,
            "name": "美团配送-光速达",
            "logo": "https://file-static.juhesaas.com/delivery/meituan.png",
            "status": -1
        }, {
            "id": "11731",
            "channelType": "731",
            "canAddTips": 0,
            "name": "美团配送-快速达",
            "logo": "https://file-static.juhesaas.com/delivery/meituan.png",
            "status": -1
        }, {
            "id": "733",
            "channelType": "733",
            "canAddTips": 1,
            "name": "达达配送",
            "logo": "https://file-static.juhesaas.com/channel/dada_logo.png",
            "status": 1
        }, {
            "id": "734",
            "channelType": "734",
            "canAddTips": 1,
            "name": "顺丰同城",
            "logo": "https://file-static.juhesaas.com/shopkeeper_admin/p046jC1658980302974.png",
            "status": -1
        }
    ],
    "message": "OK",
    "code": 0
}
```

- **_使用说明_**
  
  > 1. 此接口可选接入，若你需要指定渠道创建or更新配送店铺，则需要先调用此接口拿到配送渠道类型，再调用店铺创建or更新接口；若不指定渠道，则全量创建or更新配送店铺
  > 2. 若入参shopId为空时，则查询全量配送渠道信息；
  >    若shopId不为空时，查询全量配送渠道信息，同时携带店铺配送渠道开通状态;

---  

#### 3. **创建配送门店**

`POST` [https://open.juhesaas.com/api/delivery/shop/create](https://open.juhesaas.com/api/delivery/shop/create)

- **_请求说明_**  

| 请求参数               | 名称          | 类型       | 是否必填 | 备注                                  |
| ------------------ | ----------- | -------- | ---- | ----------------------------------- |
| businessCategoryId | 店铺业态id      | int      | 是    | 数据来源于接口：查询配送门店业务类型                  |
| name               | 店铺名称        | string   | 是    |                                     |
| provinceCode       | 地址-省code    | string   | 是    | 使用标准行政区划代码                          |
| cityCode           | 地址-市code    | string   | 是    | 使用标准行政区划代码                          |
| districtCode       | 地址-区code    | string   | 是    | 使用标准行政区划代码                          |
| address            | 详细地址        | string   | 是    | 详细地址（全路径）                           |
| longitude          | 经度          | double   | 是    | 使用火星地图                              |
| latitude           | 纬度          | double   | 是    | 使用火星地图                              |
| contactName        | 联系人姓名       | string   | 是    |                                     |
| telephone          | 联系人手机号      | string   | 是    |                                     |
| authChannelList    | 需要开通的渠道类型列表 | string[] | 否    | 数据来源于接口：2. 查询配送渠道列表 字段[channelType] |

 _示例：_

```java
{
    "name": "wish测试店铺071905",
    "businessCategoryId": 1,
    "provinceCode": 420000,
    "cityCode": 420300,
    "districtCode": 420324,
    "address": "来自未知的0085号星球",
    "longitude": "113.886844",
    "latitude": "22.572666",
    "contactName": "小王子",
    "telephone": "13713794904",
    "authChannelList": [733, 731]
}
```

- **_响应说明_**  

| 响应参数                  | 名称       | 类型     | 备注                                                                |
| --------------------- | -------- | ------ | ----------------------------------------------------------------- |
| code                  | 响应码      | int    | 0 表示请求成功，其它表示失败                                                   |
| message               | 响应消息     | string | 响应码对应的说明                                                          |
| data                  |          |        |                                                                   |
| - shopId              | 配送店铺id   | long   | 只要店铺创建成功即会返回                                                      |
| - channelAuthFailList | 渠道开通失败信息 | list   | 当某个渠道开通失败时，返回对应的失败信息。<br>您可以调用使用返回的shopId调用更新配送门店接口（指定失败的渠道）来重试开通 |
| -- shopId             | 店铺id     | long   |                                                                   |
| -- channelType        | 渠道类型     | string |                                                                   |
| -- channelName        | 渠道名称     | string |                                                                   |
| -- failReason         | 失败信息     | string |                                                                   |

_示例：_

```java
{
    "data": {
        "shopId": "1365",
        "channelAuthFailList": [
            {
                "shopId": 1365,
                "channelType": 734,
                "channelName": "顺丰同城",
                "failReason": "Connection refused: connect"
            }
        ]
    },
    "message": "OK",
    "code": 0
}
```

- **_使用说明_**
  
  > 1. 店铺业态不可更改，请谨慎选择贴合自身业务的类型
  > 2. 请及时保存接口返回的shopId，当出现渠道开通失败信息时，可使用此shopId调用更新配送门店接口，从而重试开通失败的渠道
  > 3. 仅当你方没有shopId时，需调用此接口；之后如果需要新增开通其他配送渠道，或者更新配送店铺，都调用更新配送门店接口
  > 4. shopId与配送渠道是一对多的关系
  > 5. 同一账户下，店铺名称不可重复

---

#### 4. **更新配送门店**

`POST` [https://open.juhesaas.com/api/delivery/shop/update](https://open.juhesaas.com/api/delivery/shop/update)

- ***请求说明***

| 请求参数               | 名称              | 类型     | 是否必填 | 备注                 |
| ------------------ | --------------- | ------ | ---- | ------------------ |
| shopId             | 店铺id            | long   | 是    | 店铺id               |
| businessCategoryId | 店铺业态id          | int    | 是    | 数据来源于接口：查询配送门店业务类型 |
| name               | 店铺名称            | string | 是    |                    |
| provinceCode       | 地址-省code        | string | 是    | 使用标准行政区划代码         |
| cityCode           | 地址-市code        | string | 是    | 使用标准行政区划代码         |
| districtCode       | 地址-区code        | string | 是    | 使用标准行政区划代码         |
| address            | 详细地址            | string | 是    | 详细地址（全路径）          |
| longitude          | 经度              | double | 是    | 使用火星地图             |
| latitude           | 纬度              | double | 是    | 使用火星地图             |
| contactName        | 联系人姓名           | string | 是    |                    |
| telephone          | 联系人手机号          | string | 是    |                    |
| authChannelList    | 需要开通or更新的渠道id列表 | list   | 否    | 数据来源于接口：查询配送渠道列表   |

*示例：*

```java
{
    "shopId":"985",
    "name": "wish测试店铺071905",
    "businessCategoryId": 1,
    "provinceCode": 420000,
    "cityCode": 420300,
    "districtCode": 420324,
    "address": "来自未知的0085号星球",
    "longitude": "113.886844",
    "latitude": "22.572666",
    "contactName": "小王子",
    "telephone": "13713794904",
    "authChannelList": [
        733
    ]
}
```

- ***响应说明***

| 响应参数                  | 名称           | 类型     | 备注                                                                  |
| --------------------- | ------------ | ------ | ------------------------------------------------------------------- |
| code                  | 响应码          | int    | 0 表示请求成功，其它表示失败                                                     |
| message               | 响应消息         | string | 响应码对应的说明                                                            |
| data                  |              |        |                                                                     |
| - shopId              | 配送店铺id       | long   |                                                                     |
| - channelAuthFailList | 渠道开通or更新失败信息 | list   | 当某个渠道开通or更新失败时，返回对应的失败信息。<br>您可以使用返回的shopId调用更新配送门店接口（指定失败的渠道）来重试开通 |
| -- shopId             | 店铺id         | long   |                                                                     |
| -- channeType         | 渠道类型         | string |                                                                     |
| -- channelName        | 渠道名称         | string |                                                                     |
| -- failReason         | 失败信息         | string |                                                                     |

*示例：*

```java
{
    "data": {
        "shopId": "1365",
        "channelAuthFailList": [
            {
                "shopId": 1365,
                "channelType": 734,
                "channelName": "顺丰同城",
                "failReason": "Connection refused: connect"
            }
        ]
    },
    "message": "OK",
    "code": 0
}
```

- ***使用说明***
  
  > 1. businessCategoryId传参仅做验证，实际并不会修改
  > 2. 当出现渠道开通失败信息时，可根据此信息，修改入参重试接口
  > 3. 同一账户下，店铺名称不可重复

---

#### 5. **查询配送门店信息**

`POST` [https://open.juhesaas.com/api/delivery/shop/info](https://open.juhesaas.com/api/delivery/shop/info)

- ***请求说明***

| 请求参数   | 名称   | 类型   | 是否必填 | 备注   |
| ------ | ---- | ---- | ---- | ---- |
| shopId | 店铺id | long | 是    | 店铺id |

*示例：*

```java
{
    "shopId":"985"
}
```

- ***响应说明***

| 请求参数                 | 名称       | 类型     | 备注              |
| -------------------- | -------- | ------ | --------------- |
| code                 | 响应码      | int    | 0 表示请求成功，其它表示失败 |
| message              | 响应消息     | string | 响应码对应的说明        |
| data                 |          |        |                 |
| - shopId             | 店铺id     | long   |                 |
| - businessCategoryId | 店铺业态id   | int    |                 |
| - name               | 店铺名称     | string |                 |
| - provinceCode       | 地址-省code | string | 使用标准行政区划代码      |
| - cityCode           | 地址-市code | string | 使用标准行政区划代码      |
| - districtCode       | 地址-区code | string | 使用标准行政区划代码      |
| - address            | 详细地址     | string | 详细地址（全路径）       |
| - longitude          | 经度       | double | 使用火星地图          |
| - latitude           | 纬度       | double | 使用火星地图          |
| - contactName        | 联系人姓名    | string |                 |
| - telephone          | 联系人手机号   | string |                 |

*示例：*

```java
{
    "data": {
        "shopId": "1357",
        "businessCategoryId": 2,
        "name": "wish测试店铺0719",
        "provinceCode": 420000,
        "cityCode": 420300,
        "districtCode": 420324,
        "address": "来自未知的0085号星球1",
        "longitude": 113.886844,
        "latitude": 22.572666,
        "contactName": "小王子2",
        "telephone": "13713794903"
    },
    "message": "OK",
    "code": 0
}
```

- ***使用说明***

---

#### 6. **获取门店余额充值链接**

`POST` [https://open.juhesaas.com/api/delivery/shop/balance/getRechargeUrl](https://open.juhesaas.com/api/delivery/shop/balance/getRechargeUrl)

- ***请求说明***

| 请求参数           | 名称     | 类型   | 是否必填 | 备注                            |
| -------------- | ------ | ---- | ---- | ----------------------------- |
| shopId         | 店铺id   | long | 是    | 店铺id                          |
| rechargeAmount | 充值金额   | int  | 是    | 单位分                           |
| deviceOsType   | 支付设备类型 | int  | 是    | 1：IOS  <br>2：安卓 <br> 3：PC端web |

*示例：*

```java
{
    "shopId":"985",
    "rechargeAmount":"10000",
    "deviceOsType":"1"
}
```

- ***响应说明***

| 请求参数      | 名称   | 类型     | 备注              |
| --------- | ---- | ------ | --------------- |
| code      | 响应码  | int    | 0 表示请求成功，其它表示失败 |
| message   | 响应消息 | string | 响应码对应的说明        |
| data      |      |        |                 |
| - payPath | 支付链接 | string | 支付链接            |

*示例：*

```java
{
    "data": {
        "payPath":"https://nest-h5-qa001.juhesaas.com/pages/h5/pay/unifiedPay?sourceChannel=xxxx&payPathId=xxxx&sign=xxx"
    },
    "message": "OK",
    "code": 0
}
```

- ***使用说明***
  
  > 1. 金额范围1 - 5000000
  > 2. 支付链接有效期5分钟，请尽快使用；若过期，需要重新调用接口获取新支付链接
  > 3. 支付链接使用方式：可直接跳转页面使用

---

#### 7. **查询门店账户余额**

`POST` [https://open.juhesaas.com/api/delivery/shop/balance/info](https://open.juhesaas.com/api/delivery/shop/balance/info)

- ***请求说明***

| 请求参数   | 名称   | 类型   | 是否必填 | 备注   |
| ------ | ---- | ---- | ---- | ---- |
| shopId | 店铺id | long | 是    | 店铺id |

*示例：*

```java
{
    "shopId":"985"
}
```

- ***响应说明***

| 请求参数            | 名称     | 类型     | 备注                    |
| --------------- | ------ | ------ | --------------------- |
| code            | 响应码    | int    | 0 表示请求成功，其它表示失败       |
| message         | 响应消息   | string | 响应码对应的说明              |
| data            |        |        |                       |
| - balanceAmount | 可用余额   | int    | 可用余额 = 总余额 - 冻结金额，单位分 |
| - freezeAmount  | 余额冻结金额 | int    | 业务流程处理中，临时冻结的金额，单位分   |
| - accountStatus | 账户状态   | int    | 账户状态: 0未开通 1开通 2冻结    |

*示例：*

```java
{
    "data": {
        "shopId": 1357,
        "balanceAmount": 2000,
        "freezeAmount":500,
        "accountStatus": 1
    },
    "message": "OK",
    "code": 0
}
```

- ***使用说明***
  
  > 1. 账户状态解释：<br>0未开通（调用创建配送店铺，接口成功即会自动开通店铺的余额账户）<br>1开通（可正常使用）<br>2冻结 （账户冻结，暂不可用，可能是处于特殊业务流程中，例如：提现）

---

### 配送单管理

##### 1. **配送估价**

   `POST` [https://open.juhesaas.com/api/delivery/estimate](https://open.juhesaas.com/api/delivery/estimate)

   **请求说明:**

| 请求参数                    | 名称                                       | 类型       | 取值范围       | 是否必填 |
| ----------------------- | ---------------------------------------- | -------- | ---------- | ---- |
| shopId                  | 店铺id                                     | long     |            | 是    |
| deliveryServiceIds      | 选择的配送服务ID                                | string[] | 见：查询配送服务列表 | 否    |
| receiverMobile          | 收件人手机号码                                  | string   | 30         | 是    |
| receiverName            | 收件人姓名                                    | string   | 31         | 是    |
| receiverAddress         | 收件人地址                                    | string   | 200        | 是    |
| receiverLng             | 收件人定位经度                                  | double   | (0, )      | 是    |
| receiverLat             | 收件人定位纬度                                  | double   | (0, )      | 是    |
| salesOrderNo            | 调用方平台内的交易订单号，方便骑手取货时核对信息，要求在同一个appid下唯一                     | string   | [5, 50]    | 是    |
| salesOrderSourceDaySn   | 调用方平台内的交易订单日流水号，方便骑手取货时核对信息                                 | int      | [1, )      | 是    |
| salesOrderSourceChannel | 订单的来源渠道ID                                | long     | 见：查询售卖渠道列表 | 是    |
| salesOrderSource        | 订单的来源渠道名称，条件必填，salesOrderSourceChannel=0 | string   | 20         | 否    |
| productNum              | 商品数量，默认 1                                | int      | [1, )      | 否    |
| productWeight           | 商品总重量（g），业务上如果不足1000g按1000g算，不填自动按各配送渠道的最小重量计价                 | int      | [1000, )      | 否    |
| productAmount           | 商品总金额 (分)                                   | int      | [1, )      | 是    |
| directDelivery          | 是否直达：0否, 1是；选择直拿直送后，同一时间骑士只能配送此订单至完成，同时，也会相应的增加配送费用，目前顺丰、达达支持直达    | int      | (0\|1)      | 否    |
| inventoryList           | 商品清单                                     | object[] |            | 是    |
| - productNo             | 商品编码                                     | string   | 50         | 是    |
| - productName           | 商品名称                                     | string   | 100        | 是    |
| - unit                  | 商品单位                                     | string   | 50         | 否    |
| - quantity              | 购买时的sku数量                                | int      | 1          | 是    |
| - skuProperty           | 商品规格                                     | string   | 200        | 否    |
| - extraProperty         | 商品附加属性                                   | string   | 200        | 否    |

   *示例：*

```json
{
"receiverMobile" : "180000000000",
"receiverName" : "李小姐",
"receiverAddress" : "深圳市南山区仙茶路与兴科路交叉路口往东北约100米大疆天空之城",
"shopId" : 102901,
"receiverLng" : 113.949217,
"receiverLat" : 22.582996,
"salesOrderNo" : "A00041",
"salesOrderSourceChannel": 0,
"salesOrderSource": "美团",
"salesOrderSourceDaySn": "1",
"deliveryServiceIds": [733],
"notifyUrl": "http://localhost/callback/test",
"productAmount" : 10000,
"productWeight": 1,
"inventoryList": [{
    "productNo":"P000001",
    "productName":"粽子",
    "unit":"个",
     "quantity":1,
                "skuProperty":"肉粽",
                "extraProperty": "咸香"
    }]
}
```

- **响应说明:**
  
  | 响应参数                  | 名称           | 类型       | 备注              |
  | --------------------- | ------------ | -------- | --------------- |
  | code                  | 响应码          | int      | 0 表示请求成功，其它表示失败 |
  | message               | 响应消息         | string   | 响应码对应的说明        |
  | data                  |              |          |                 |
  | - valuationList       | 估算结果列表       | object[] |                 |
  | - deliveryChannel     | 账户状态         | string   |                 |
  | - deliveryChannelName | 配送渠道名称       | string   |                 |
  | - deliveryChannelLogo | 配送渠道logo url | string   |                 |
  | - deliveryAmount      | 配送费(分)       | int      |                 |
  | - distance            | 配送距离(米)      | int      |                 |
  | - estimateErrorMsg    | 渠道估值错误信息     | string   |                 |
  
  *示例：*
  
  ```json
  {
    "data": {
        "valuationList": [{
                "deliveryChannel": "733",
                "deliveryChannelName": "达达配送",
                "deliveryChannelLogo": "https://file-static.juhesaas.com/channel/dada_logo.png",
                "deliveryAmount": 500,
                "distance": 24080
            },{
                "deliveryChannel": "734",
                "deliveryChannelName": "顺丰配送",
                "deliveryChannelLogo": "https://file-static.juhesaas.com/channel/s_logo.png",
                "deliveryAmount": 600,
                "distance": 24080
            }
  
        ]
    },
    "message": "OK",
    "code": 0
  }
  ```

---

##### 2. **发配送单**

   `POST` [https://open.juhesaas.com/api/delivery/dispatch](https://open.juhesaas.com/api/delivery/dispatch)

- **请求说明:**

| 请求参数                    | 名称                                       | 类型       | 取值范围       | 是否必填 |
| ----------------------- | ---------------------------------------- | -------- | ---------- | ---- |
| shopId                  | 店铺id                                     | long     |            | 是    |
| deliveryServiceIds      | 选择的配送服务ID                                | long[]   | 见：查询配送服务列表 | 否    |
| receiverMobile          | 收件人手机号码                                  | string   | 30         | 是    |
| receiverName            | 收件人姓名                                    | string   | 31         | 是    |
| receiverAddress         | 收件人地址                                    | string   | 200        | 是    |
| receiverLng             | 收件人定位经度                                  | double   | (0, )      | 是    |
| receiverLat             | 收件人定位纬度                                  | double   | (0, )      | 是    |
| salesOrderNo            | 调用方平台内的交易订单号，方便骑手取货时核对信息，要求在同一个appid下唯一。                     | string   | [5, 50]    | 是    |
| salesOrderSourceDaySn   | 业务订单日流水号，调用方平台内的交易订单日流水号，方便骑手取货时核对信息用                           | int      | [1, )      | 是    |
| salesOrderSourceChannel | 订单的来源渠道ID                                | long     | 见：查询售卖渠道列表 | 是    |
| salesOrderSource        | 订单的来源渠道名称，条件必填，salesOrderSourceChannel=0 | string   | 20         | 否    |
| productNum              | 商品数量，默认 1                                | int      | [1, )      | 否    |
| productWeight           | 商品总重量（g），业务上如果不足1000g按1000g算，不填自动按各配送渠道的最小重量计价                 | int      | [1, )      | 否    |
| productAmount           | 商品总金额 (分)                                    | int      | [1, )      | 是    |
| directDelivery          | 是否直达：0否, 1是；选择直拿直送后，同一时间骑士只能配送此订单至完成，同时，也会相应的增加配送费用，目前顺丰、达达支持直达    | int      | (0\|1)      | 否    |
| remark           		  | 发单备注，此内容会显示到骑手接单页面                | string      | (0, 500]      | 否    |
| inventoryList           | 商品清单                                     | object[] |            | 是    |
| - productNo             | 商品编码                                     | string   | 50         | 是    |
| - productName           | 商品名称                                     | string   | 100        | 是    |
| - unit                  | 商品单位                                     | string   | 50         | 否    |
| - quantity              | 购买时的sku数量                                | int      | 1          | 否    |
| - skuProperty           | 商品规格                                     | string   | 200        | 否    |
| - extraProperty         | 商品附加属性                                   | string   | 200        | 否    |
| notifyUrl               | 通知URL，不填就不回调，回调数据结构见回调通知一节               | string   | 255        | 否    |

*示例：*

```java
{
"receiverMobile" : "180000000000",
"receiverName" : "李小姐",
"receiverAddress" : "深圳市南山区仙茶路与兴科路交叉路口往东北约100米大疆天空之城",
"shopId" : 102901,
"receiverLng" : 113.949217,
"receiverLat" : 22.582996,
"salesOrderNo" : "A00041",
"salesOrderSourceChannel": 0,
"salesOrderSource": "美团",
"salesOrderSourceDaySn": "1",
"deliveryServiceIds": ["733"],
"notifyUrl": "http://localhost/callback/test",
"productAmount" : 10000,
"productWeight": 1,
"inventoryList": [{
    "productNo":"P000001",
    "productName":"粽子",
    "unit":"个",
     "quantity":1,
           "skuProperty":"肉粽",
           "extraProperty": "咸香"
    }]
}
```

- **响应说明**
  
  | 请求参数    | 名称   | 类型     | 备注              |
  | ------- | ---- | ------ | --------------- |
  | code    | 响应码  | int    | 0 表示请求成功，其它表示失败 |
  | message | 响应消息 | string | 响应码对应的说明        |
  
  *示例：*
  
  ```json
  {
    "message": "OK",
    "code": 0
  }
  ```
  
  ##### 3. **给骑手小费**
  
   `POST` [https://open.juhesaas.com/api/delivery/addTips](https://open.juhesaas.com/api/delivery/addTips)

- *请求说明*
  
  | 请求参数             | 名称                       | 类型       | 取值范围       | 是否必填 |
  | ---------------- | ------------------------ | -------- | ---------- | ---- |
  | shopId           | 店铺id                     | long     |            | 是    |
  | salesOrderNo     | 发配送单时请求的调用方平台内的交易订单号                  | string   | 50         | 是    |
  | tipsAmountYuan   | 要给骑手的总小费金额（非增量）(元)              | int      | [1,20]     | 是    |
  | deliveryServices | 已发配送服务ID列表，不传代表给所有渠道骑手小费 | string[] | 见：查询配送服务列表 | 否    |
  
  注意点：
	 1、小费的金额不能比订单商品金额还高;
     2、多次调整给骑手小费金额时，后一次的小费金额应该大于前一次给的小费金额
	 
  
  *请求示例：*
  
  ```json
  {
  "shopId" : 102901,
  "salesOrderNo" : "A00041",
  "tipsAmountYuan": 3,
  "deliveryServices": ["733","734"]
  }
  ```

- *响应说明*
  
  | 请求参数    | 名称   | 类型     | 备注              |
  | ------- | ---- | ------ | --------------- |
  | code    | 响应码  | int    | 0 表示请求成功，其它表示失败 |
  | message | 响应消息 | string | 响应码对应的说明        |
  | data |  | object[] | 小费添加结果        |
  |  - deliveryChannel | 配送渠道 | int |         |
  |  - status | 添加小费成功失败状态：1 成功, 2 没有添加/添加失败 | int |         |
  |  - errorMsg | 增加失败原因 | String |         |
  |  - tipsAmountYuan | 当前渠道给骑手的总小费金额(元) | int |         |
  
  
  *响应示例：*
  
 
  ```json
	{
		"data": [{
				"deliveryChannel": 733,
				"status": 2,
				"errorMsg": "添加小费失败: 小费金额不能大于订单金额",
				"tipsAmountYuan": 0
			},
			{
				"deliveryChannel": 734,
				"status": 1,
				"tipsAmountYuan": 3
			}
		],
		"message": "OK",
		"code": 0,
		"rid": 95439732035607
	}

  ```

---

##### 4. **取消配送**

   `POST` [https://open.juhesaas.com/api/delivery/cancel](https://open.juhesaas.com/api/delivery/cancel)

- *请求说明*
  
  | 请求参数           | 名称                                | 类型     | 取值范围       | 是否必填 |
  | -------------- | --------------------------------- | ------ | ---------- | ---- |
  | shopId         | 店铺id                              | long   |            | 是    |
  | salesOrderNo   | 发配送单时传入调用方平台内的交易订单号                           | string | 50         | 是    |
  | cancelReasonId | 取消原因ID                            | int    |            | 是    |
  | cancelReason   | 取消原因说明，原因为其它时必填（cancelReasonId=0） | string | 见：查询配送渠道列表 | 否    |
  
  *示例：*
  
  ```json
  {
      "shopId": 102901,
      "salesOrderNo": "A00041",
      "cancelReasonId": 0,
      "cancelReason": "test"
  }  
  ```

- *响应说明*
  
  | 请求参数    | 名称   | 类型     | 备注              |
  | ------- | ---- | ------ | --------------- |
  | code    | 响应码  | int    | 0 表示请求成功，其它表示失败 |
  | message | 响应消息 | string | 响应码对应的说明        |
  
  *示例：*
  
  ```json
  {
    "message": "OK",
    "code": 0
  }
  ```

---

##### 5. **查询骑手实时位置**

   `POST` [https://open.juhesaas.com/api/delivery/getCurrentLocation](https://open.juhesaas.com/api/delivery/getCurrentLocation)

- *请求说明*
  
  | 请求参数         | 名称      | 类型     | 取值范围 | 是否必填 |
  | ------------ | ------- | ------ | ---- | ---- |
  | shopId       | 店铺id    | long   |      | 是    |
  | salesOrderNo | 发配送单时请求的调用方平台内的交易订单号 | string | 50   | 是    |
  
  *示例：*
  
  ```json
  {
      "shopId": 102901,
      "salesOrderNo": "A00041"
  } 
  ```

- *响应说明*
  
  | 请求参数               | 名称   | 类型     | 备注              |
  | ------------------ | ---- | ------ | --------------- |
  | code               | 响应码  | int    | 0 表示请求成功，其它表示失败 |
  | message            | 响应消息 | string | 响应码对应的说明        |
  | data               |      |        |                 |
  | - deliverymanName  | 骑手名称 | string |                 |
  | - deliverymanPhone | 骑手号码 | string |                 |
  | - lat              | 当前纬度 | double |                 |
  | - lng              | 当前纬度 | double |                 |
  
  *示例：*
  
  ```json
  {
    "message": "OK",
    "code": 0
    "data": {
      "lat": 22.582996,
      "lng": 113.949217,
      "deliverymanName": "骑手名称",
      "deliverymanPhone": "18000000000",
    }
  }
  ```

---

##### 6. **获取骑手位置追踪H5页面**

   `POST` [https://open.juhesaas.com/api/delivery/getRiderTraceLink](https://open.juhesaas.com/api/delivery/getRiderTraceLink)
   
   支持H5的方式追加骑手位置的配送服务附录说明

- *请求说明*
  
  | 请求参数         | 名称      | 类型     | 取值范围 | 是否必填 |
  | ------------ | ------- | ------ | ---- | ---- |
  | shopId       | 店铺id    | long   |      | 是    |
  | salesOrderNo | 发配送单时请求的调用方平台内的交易订单号 | string | 50   | 是    |
  
  *示例：*
  
  ```json
  {
      "shopId": 102901,
      "salesOrderNo": "A00041"
  } 
  ```

  
  
- *响应说明*
  
  | 请求参数               | 名称   | 类型     | 备注              |
  | ------------------ | ---- | ------ | --------------- |
  | code               | 响应码  | int    | 0 表示请求成功，其它表示失败 |
  | message            | 响应消息 | string | 响应码对应的说明        |
  | data               |      |        |                 |
  | - link  | h5 URL连接地址 | string |                 |
  
  *示例：*
  
  ```json
  {
    "message": "OK",
    "code": 0
    "data": {
      "link": "http://deliveryChannelDomain/feedback/home/?signature=2RCXef5+2ZqCZQFmoKYaOeci7Xx0R02ZgM6TLZZqtgY="
    }
  }
  ```

---

##### 7. **查询取消配送原因列表**

   `POST` [https://open.juhesaas.com/api/delivery/getCancelReasons](https://open.juhesaas.com/api/delivery/getCancelReasons)

- *请求说明*
  
  | 请求参数 | 名称  | 类型  | 取值范围 | 是否必填 |
  | ---- | --- | --- | ---- | ---- |
  |      |     |     |      |      |
  
  *示例：*
  
  ```json
  {}
  ```

- *响应说明*
  
  | 请求参数    | 名称   | 类型       | 备注              |
  | ------- | ---- | -------- | --------------- |
  | code    | 响应码  | int      | 0 表示请求成功，其它表示失败 |
  | message | 响应消息 | string   | 响应码对应的说明        |
  | data    | 原因列表 | object[] |                 |
  | - id    | 原因ID | string   |                 |
  | - desc  | 原因说明 | string   |                 |
  
  *示例：*
  
  ```json
  {
      "data": [{
              "id": "1",
              "desc": "没有配送员接单"
          }, {
              "id": "2",
              "desc": "配送员没来取货"
          }, {
              "id": "3",
              "desc": "配送员态度太差"
          }, {
              "id": "4",
              "desc": "顾客取消订单"
          }, {
              "id": "5",
              "desc": "订单填写错误"
          }, {
              "id": "6",
              "desc": "配送员让我取消此单"
          }, {
              "id": "7",
              "desc": "配送员不愿上门取货"
          }, {
              "id": "8",
              "desc": "我不需要配送了"
          }, {
              "id": "9",
              "desc": "配送员以各种理由表示无法完成订单"
          }, {
              "id": "10",
              "desc": "被其他平台抢单"
          }, {
              "id": "11",
              "desc": "暂时无法提供待配送物品"
          }, {
              "id": "12",
              "desc": "重复下单，取消此单"
          }, {
              "id": "0",
              "desc": "其他"
          }
      ],
      "message": "OK",
      "code": 0
  }
  ```

---

##### 8. **查询售卖渠道列表**

   `POST` https://open.juhesaas.com/api/delivery/getSalesOrderChannels

- *请求说明*
  
  | 请求参数 | 名称  | 类型  | 取值范围 | 是否必填 |
  | ---- | --- | --- | ---- | ---- |
  |      |     |     |      |      |
  
  *示例：*
  
  ```json
  {}
  ```

- *响应说明*
  
  | 参数      | 名称       | 类型       | 备注              |
  | ------- | -------- | -------- | --------------- |
  | code    | 响应码      | int      | 0 表示请求成功，其它表示失败 |
  | message | 响应消息     | string   | 响应码对应的说明        |
  | data    | 业务订单渠道列表 | object[] |                 |
  | - id    | 订单渠道ID   | string   |                 |
  | - desc  | 订单渠道名称   | string   |                 |
  
  *示例：*
  
  ```json
  {
      "data": [{
              "id": "131",
              "desc": "美团外卖"
          }, {
              "id": "132",
              "desc": "饿了么"
          }, {
              "id": "133",
              "desc": "京东到家"
          }, {
              "id": "134",
              "desc": "饿百零售"
          }, {
              "id": "135",
              "desc": "美团闪购"
          }, {
              "id": "136",
              "desc": "抖音"
          }, {
              "id": "137",
              "desc": "有赞"
          }, {
              "id": "138",
              "desc": "微盟"
          }, {
              "id": "139",
              "desc": "拼多多"
          }, {
              "id": "140",
              "desc": "口碑"
          }, {
              "id": "401",
              "desc": "其他平台"
          }
      ],
      "message": "OK",
      "code": 0
  }
  ```

---

### 回调：

| 业务类型 | 备注        |
| ---- | --------- |
| 4    | 配送单状态变更事件 |

#### 响应示例：

```json
{
    "type": 4, // 见附录回调业务类型
    "rtime": 1662459209, // 回调时间
    "data": "{\"shopId\":6308559984,\"salesOrderNo\":\"A00062\",\"status\":60,\"deliverymanName\":\"甜甜田超辉\",\"deliverymanMobile\":\"18521736087\",\"distance\":1,\"deliveryAmount\":100,\"couponAmount\":0,\"tipsAmount\":300,\"insuranceAmount\":0,\"payAmount\":100,\"deductAmount\":0,\"timestamp\":1637720013}"
}
```

#### 回调data属性说明：

[O] 选填，视条件返回
[M] 一定有值；

```json
{
    "shopId": 6308559984,  // [M] 门店ID
    "salesOrderNo": "A00062", // [M] 
    "event": "17", //  [M] 事件编码，在处理转单事件时尤为有用， 具体见附录: 配送单回调事件编码
    "status": 60,  // [M] 配送单状态，见附录配送单状态
	"deliveryChannel": 733 // [M] 已收单渠道, 见附录配送渠道列表，第三方配送平台未接单前返回0
	"deliveryServiceId": 733 // [M] 已收单的配送服务, 见附录配送服务列表，第三方配送平台未接单前返回0
    "deliverymanName": "甜甜田超辉", // [O] 配送员名称，按配送单状态返回，骑手接单后返回
    "deliverymanMobile": "18521736087", // [O] 配送员电话，按视配送单状态返回，骑手接单后返回
	"lat": 23.001341, // [O] 物品当前最新位置，纬度
	"lng": 116.13433, // [O] 物品当前最新位置，经度
    "distance": 1, // [O] 配送距离(米)，
    "deliveryAmount": 100, // [M] 配送费原价(分)
    "couponAmount": 0, // [M] 优惠金额(分)，配送单符合第三方配送平台相关活动自动享受到优惠金额
    "tipsAmount": 300, // [M] 小费金额(分)
    "insuranceAmount": 0, // [M] 保价费金额(分)
    "payAmount": 100, // [M] 实付配送金额 = 配送费原价 - 优惠
    "deductAmount": 0, // [O] 违约金(分)，取消事件回调时返回，骑手接单后一段时间非骑手原因取消配送产生的违约金
    "timestamp": 1637720013 // [M] 业务发生时间
}
```

-----

### 附录：

#### 配送单状态表：

| status | 状态名称       | 是否回调消息  | 备注  |
| ------ | ---------- | --- | --- |
| 0      | 待平台接单      |   否  |  此步骤会把订单派单给指定的所有第三方配送渠道   |
| 10     | 待骑手接单      |  是   |  在此状态时还没有确认配送平台，目前聚合配送采用的是骑手竟单的方式来确认第三方配送渠道，哪个渠道的骑手先接单就是安排哪个渠道送，未接单或晚接单的渠道会被取消   |
| 20     | 待骑手取货      |  是   |  在此状态时订单已确认了配送渠道，回调消息中会包含配送渠道及接单的骑手信息   |
| 30     | 取货中        |  否   |     |
| 40     | 配送中        |  是   |  骑手取到货   |
| 60     | 已完成        |  是   |   物品送达到的用户手上  |
| 70     | 已取消        |  是   |   配送被取消，会返回被取消原因(cancelReason、cancelReasonId)和可能产生的违约金额(deductAmount)，扣违约金规则视第三方配送渠道不同有所有一样，一般是在骑手接单后短时间内取消是不扣违约金  |
| 71     | 取消中        |  否   |     |
| 80     | 妥投异常物品返回中  |  否   |     |
| 90     | 妥投异常物品返回完成 |   是  |   针对骑手联系不上顾客或顾客拒收，骑手返单的场景  |
| 100    | 派单失败       |   是  |  罕见，进入该状态往往是所选第三方派单平台都出现故障造成派单失败才会出现  |


注意：当骑手转单时，会以相同的状态+骑手信息进行回调，业务上请做到幂等判断


#### 异常编码表：

| code | message                    | remark |
| ---- | --------------------------:|:------:|
| 1901 | 操作失败，配送流程已流转结束             |        |
| 1902 | 配送单不存在                     |        |
| 1903 | 配送单已流转结束                   |        |
| 1907 | 配送单状态已发生改变                 |        |
| 1909 | 取消配送单失败                    |        |
| 1910 | 当前订单的配送单状态不支持该操作           |        |
| 1911 | 门店未开通相关配送平台                |        |
| 1912 | 配送平台已绑定                    |        |
| 1913 | 该收获地址不存在                   |        |
| 1914 | 该门店未开通配送                   |        |
| 1915 | 配送渠道店铺不存在                  |        |
| 1918 | 余额不足                       |        |
| 1921 | 只能在等待骑手接单时增加小费             |        |
| 1922 | 添加小费失败                     |        |
| 1923 | 勿重复发配送                     |        |
| 1924 | 包含不支持的配送渠道                 |        |
| 1927 | 首次开通配送，店铺信息不可为空            |        |
| 1928 | 配送渠道不存在                    |        |
| 1930 | 发单失败                       |        |
| 1931 | 查询配送单位置失败\|只有骑手接单后才能查询位置信息 |        |
| 1932 | 店铺业务类型不支持修改                |        |
| 1933 | 确认收到返运商品时发生异常              |        |

#### 配送渠道服务列表：

| 配送服务       | 配送服务编码 | 配送渠道 | 配送渠道编码 | 是否支持增加小费 | 是否支持H5追踪骑手位置 | 备注 |
| ---------- | ------ | ---- | ------ | -------- | ----- | ----- |
| 美团配送 - 快速达 | 11731  | 美团   | 731    | 否        |  是        | 已停止服务 |
| 美团配送 - 光速达 | 10731  | 美团   | 731    | 否        | 是        |  已停止服务 |
| 达达配送       | 733    | 达达   | 733    | 是        | 是        |  |
| 顺丰同城       | 734    | 顺丰   | 734    | 是        | 是        |  |
| UU跑腿       | 735    | UU跑腿   | 735    | 是        | 否        |  |
| 闪送       | 736    | 闪送   | 736    | 是        | 否        |  隶属北京闪送科技有限公司旗下品牌，不同于美团闪送 |


#### 配送单回调事件编码：


| code | message                    | remark |
| ---- | --------------------------:|:------:|
| 17 | 骑手发起了转单            |   骑手接单之后又取消，第三方配送平台按排其他骑手配送，中间时间不确定或短或长，在收到转单事件，商家需要及时关注     |
